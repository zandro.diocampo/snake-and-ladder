package ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder.GameActivity.Companion.EXTRA_NUM_PLAYERS
import ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder.GameActivity.Companion.EXTRA_PLAYER_NAMES
import ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private var numPlayers = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val singlePlayerButton = view.findViewById<Button>(R.id.single_player_button)
        singlePlayerButton.setOnClickListener {
            startNewGame(1)
        }

        val twoPlayersButton = view.findViewById<Button>(R.id.two_players_button)
        twoPlayersButton.setOnClickListener {
            startNewGame(2)
        }

        val threePlayersButton = view.findViewById<Button>(R.id.three_players_button)
        threePlayersButton.setOnClickListener {
            startNewGame(3)
        }

        val fourPlayersButton = view.findViewById<Button>(R.id.four_players_button)
        fourPlayersButton.setOnClickListener {
            startNewGame(4)
        }

        return view
    }

    private fun startNewGame(numPlayers: Int) {
        // Prompt the user to enter the names of the players
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Enter Player Names")
        val inputLayout = LinearLayout(requireContext())
        inputLayout.orientation = LinearLayout.VERTICAL
        val playerNames = mutableListOf<String>()
        for (i in 1..numPlayers) {
            val input = EditText(requireContext())
            input.hint = "Player $i Name"
            inputLayout.addView(input)
            playerNames.add("Player $i")
        }
        builder.setView(inputLayout)

        builder.setPositiveButton("OK") { dialog, which ->
            // Update the player names if the user entered them
            for (i in playerNames.indices) {
                val playerName = inputLayout.getChildAt(i) as EditText
                val name = playerName.text.toString().trim()
                if (name.isNotEmpty()) {
                    playerNames[i] = name
                }
            }

            // Start a new game with the specified number of players and names
            val intent = Intent(activity, GameActivity::class.java)
            intent.putExtra(EXTRA_NUM_PLAYERS, numPlayers)
            intent.putStringArrayListExtra(EXTRA_PLAYER_NAMES, ArrayList(playerNames))
            startActivity(intent)
        }

        builder.setNegativeButton("Cancel", null)

        builder.show()
    }
}